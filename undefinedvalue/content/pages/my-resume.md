Title: My Résumé
Slug: kjresume
Alias: /page/kjresume
Date: 2017-07-04 21:16
Modified: 2017-07-22 14:26

I'm not looking for a job right now, but my résumé is available at Stack Overflow Careers: <http://careers.stackoverflow.com/kristopherjohnson>.

You can download it in various formats using these links:

- [Microsoft Word format (docx)]({filename}/files/kjresume_2017.docx)
- [Portable Document Format (PDF)]({filename}/files/kjresume_2017.pdf)
- [Rich Text Format (RTF)]({filename}/files/kjresume_2017.pdf)
- [Plain Text]({filename}/files/kjresume_2017.txt)

